#ifndef _TEST_H_
#define _TEST_H_

#include "mem.h"
#include "mem_internals.h"

int test_1(void* heap);
int test_2(void* heap);
int test_3(void* heap);
int test_4(void* heap);
int test_5(void* heap);
#endif 