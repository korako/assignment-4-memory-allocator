#include "tests.h"
#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>

#define print_error() printf("%s", "Some problems with memory allocation");
#define print_number_of_test(x) printf("TEST %s" "%s", #x, ": \n")
#define print_success_message(x) printf("Test_%s" "%s", #x, " is completed \n")
#define FIRST_SIZE_OF_BLOCK 1024
#define SECOND_SIZE_IF_BLOCK 512

int test_1(void* heap) {
    print_number_of_test(1);
    void* block = _malloc(FIRST_SIZE_OF_BLOCK);
    if (block) {
        debug_heap(stdout, heap);
        print_success_message(1);
        _free(block);
        return 1;
    }
    print_error();
    return 0;
}

int test_2(void* heap) {
    print_number_of_test(2);
    void* block_1 = _malloc(FIRST_SIZE_OF_BLOCK);
    void* block_2 = _malloc(SECOND_SIZE_IF_BLOCK);
    void* block_3 = _malloc(FIRST_SIZE_OF_BLOCK);
    void* block_4 = _malloc(SECOND_SIZE_IF_BLOCK);
    if (!(block_1 || block_2 || block_3 || block_4)) {
        print_error();
        return 0;
    }
    debug_heap(stdout, heap);
    _free(block_3);
    debug_heap(stdout, heap);
    print_success_message(2);
    _free(block_1);
    _free(block_2);
    _free(block_4);
    return 1;
}

int test_3(void* heap) {
    print_number_of_test(3);
    void* block_1 = _malloc(FIRST_SIZE_OF_BLOCK);
    void* block_2 = _malloc(SECOND_SIZE_IF_BLOCK);
    void* block_3 = _malloc(FIRST_SIZE_OF_BLOCK);
    void* block_4 = _malloc(SECOND_SIZE_IF_BLOCK);
    if (!(block_1 || block_2 || block_3 || block_4)) {
        print_error();
        return 0;
    }
    debug_heap(stdout, heap);
    _free(block_3);
    _free(block_1);
    debug_heap(stdout, heap);
    print_success_message(3);
    _free(block_2);
    _free(block_4);
    return 1;
}

int test_4(void* heap) {
    print_number_of_test(4);
    void* block_1 = _malloc(5000);
    debug_heap(stdout, heap);
    void* block_2 = _malloc(9000);
    if (!(block_1 || block_2)) {
        print_error();
        return 0;
    }
    debug_heap(stdout, heap);
    print_success_message(4);
    _free(block_1);
    _free(block_2);
    return 1;
}

int test_5(void* heap) {
    print_number_of_test(5);
    debug_heap(stdout, heap);
    void* block_1 = _malloc(9000);
    debug_heap(stdout, heap);
    struct block_header* header = (struct block_header*) (block_1 - offsetof(struct block_header, contents));
    void* next_header = header->capacity.bytes + header->contents;
    (void) mmap(next_header, 5000, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_FIXED, -1, 0);
    void* block_2 = _malloc(16384);
    debug_heap(stdout, heap);
    if (block_2 == NULL) {
        print_error();
        return 0;
    }
    print_success_message(5);
    _free(block_1);
    _free(block_2);
    munmap(next_header, 5000);
    return 1;
}