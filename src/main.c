#include "mem.h"
#include "tests.h"

#define HEAP_SIZE 8192

int main(void) {
    void* heap = heap_init(HEAP_SIZE);
    if (!heap) {
        printf("%s", "heap initialization error");
        return 0;
    }
    debug_heap(stdout, heap);
    test_1(heap);
    test_2(heap);
    test_3(heap);
    test_4(heap);
    test_5(heap);
    munmap(heap, size_from_capacity((block_capacity) {.bytes = 8192}).bytes);
}

